import getopt
import TheoreticallyOptimalStrategy
import indicators
import sys

if __name__ == "__main__":

    opts, args = getopt.getopt(sys.argv[1:], "vs:")

    verbose=False
    symbol="JPM"

    for opt, arg in opts:
        if opt == '-v':
            print("Verbose option provided")
            verbose=True
        if opt == '-s':
            symbol = arg

    if len(opts) < 1:
        #print("Default behavior with no supplied arguments")
        TheoreticallyOptimalStrategy.main(verbose=False)
        indicators.main()
    else:
        TheoreticallyOptimalStrategy.main(symbol=symbol,verbose=verbose)
        indicators.main(symbol=symbol)