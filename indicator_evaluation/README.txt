

# Project 6: Indicator Evaluation

## Instructions on how to run code

The ```testproject.py``` can be ran which generates the 5 technical indicators and their respective charts with the
  default ticker symbol "JPM." If the -v argument is passed to the program, it will additionally print out the portfolio
  statistics for the Theoretically Optimal Strategy and Benchmark portfolio that was included in the ```report.pdf.```
  A stock ticker symbol can also be supplied with the -s argument.

Examples on how to run the ```testproject.py``` file:

    python testproject.py
    OR
    python testproject.py -v
    OR
    python testproject.py -v -s NVDA

## Description of Files

### indicators.py

Contains the 5 indicator indicators used for Project 6. The default symbol is "JPM." The symbol can be
changed either in the main() function or an argument string of a stock symbol may be
passed in as illustrated in the "Instructions on how to run code" section. The start and end dates can also be modified
in the main function to specify a different time range.

### marketsimcode.py

An improved version of the market simulator that accepts a dataframe that has trades for a particular stock.

### TheoreticallyOptimalStrategy.py

Assumes that we can see the future that creates a set of trades representing the best a strategy could possibly
do during the in-sample period using JPM by default. The default stock ticker symbol can be modified from the main()
function in the file.

### testproject.py

Entry point to the project. The if “__name__” == “__main__”: section of the code will call the testPolicy function
in TheoreticallyOptimalStrategy, as well as the five indicators and the market simulator code to generate plots and
statistics. See "Instructions on how to run code" on the usage of this file when running from the command-line.